const bcrypt = require("bcrypt"); // Pour le hachage sécurisé des mots de passe
const Joi = require("joi"); // Pour la validation des données
const express = require("express");
const userModel = require("../models/user.model"); // Le modèle utilisateur
const genAuthToken = require("../utils/genAuthToken"); // Fonction pour générer le jeton d'authentification

const router = express.Router();

router.post("/", async (req, res) => {
  const schema = Joi.object({
    email: Joi.string().min(3).max(200).required().email(),
    password: Joi.string().min(6).max(200).required(),
  });

  const { error } = schema.validate(req.body);

  if (error) return res.status(400).send(error.details[0].message);

  let user = await userModel.findOne({ email: req.body.email });
  if (!user) return res.status(400).send("L'utilisateur n'existe pas...");

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword)
    return res.status(400).send("Email ou mot de passe invalide...");

  const token = genAuthToken(user);

  res.send(token);
});

module.exports = router;
