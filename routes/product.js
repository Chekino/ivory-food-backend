var express = require("express");
var router = express.Router();
const productController = require("../controllers/product.controller");
//Crée un nouveau plat
router.post("/", productController.store);
//Obtenir la liste des plats
router.get("/", productController.index);
//Modifier un plat
router.put("/:id", productController.update);
router.delete("/:id", productController.destroy);

module.exports = router;
