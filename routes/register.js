/*const bcrypt = require("bcrypt"); // Pour le hachage sécurisé des mots de passe
const Joi = require("joi"); // Pour la validation des données
const express = require("express");
const userModel = require("../models/user.model"); // Le modèle utilisateur
const genAuthToken = require("../utils/genAuthToken"); // Fonction pour générer le jeton d'authentification

const router = express.Router();
// Définition de la route POST pour créer un nouvel utilisateur
router.post("/", async (req, res) => {
  // Définition d'un schéma de validation pour les données de la requête

  const schema = Joi.object({
    name: Joi.string().min(3).max(30).required(),
    email: Joi.string().min(3).max(200).required().email(),
    password: Joi.string().min(3).max(200).required(),
  });
  // Validation des données de la requête par rapport au schéma défini

  const { error } = schema.validate(req.body);
  // Si des erreurs de validation sont présentes, renvoyer une réponse d'erreur
  if (error) return res.status(400).send(error.details[0].message);
  // Vérification si l'utilisateur avec l'email donné existe déjà dans la base de données
  let user = await userModel.findOne({ email: req.body.email });
  if (user) return res.status(400).send("Utilisateur existant...");

  user = new userModel({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  });
  // Génération d'un sel aléatoire pour le hachage du mot de passe
  const salt = await bcrypt.genSalt(10);
  // Hachage du mot de passe de l'utilisateur à l'aide du sel
  user.password = await bcrypt.hash(user.password, salt);
  // Sauvegarde de l'utilisateur haché dans la base de données
  user = await user.save();
  // Génération d'un jeton d'authentification pour l'utilisateur
  const token = genAuthToken(user);
  // Envoi du jeton en réponse
  res.send(token);
});

module.exports = router;*/

const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.controller"); // Importez le contrôleur approprié

// Route pour la création d'un utilisateur
router.post("/", userController.createUser);

// Autres routes pour la mise à jour, la suppression, etc.
// router.put("/:userId", userController.updateUser);
// router.delete("/:userId", userController.deleteUser);

module.exports = router;
