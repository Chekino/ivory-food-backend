const mongoose = require("mongoose");
const { Schema } = mongoose;
require("dotenv").config();

const userSchema = Schema({
  name: {
    type: String,
    required: true,
    minLength: 3,
    maxLength: 30,
  },

  email: {
    type: String,
    required: true,
    minLength: 3,
    maxLength: 200,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minLength: 3,
    maxLength: 1024,
  },
  // Vous pouvez ajouter d'autres informations d'utilisateur si nécessaire
});

const userModel = mongoose.model("user", userSchema);

module.exports = userModel;
