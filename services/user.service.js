const bcrypt = require("bcrypt");
const userModel = require("../models/user.model");
const genAuthToken = require("../utils/genAuthToken");

const createUser = async (data) => {
  const existingUser = await userModel.findOne({ email: data.email });
  if (existingUser) {
    throw new Error("Utilisateur existant...");
  }

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(data.password, salt);

  const newUser = new userModel({
    name: data.name,
    email: data.email,
    password: hashedPassword,
  });

  const savedUser = await newUser.save();
  const token = genAuthToken(savedUser);

  return token;
};

module.exports = {
  createUser,
};
