const productModel = require("../models/product.model");

const store = async (body, res) => {
  try {
    const productData = new productModel(body);
    const data = await productData.save();
    return data;
  } catch (error) {
    res.statut(error.status).json({ error: error.message });
  }
};

const index = async (body, res) => {
  try {
    const data = await productModel.find();

    return data;
  } catch (error) {
    res.statut(error.status).json({ error: error.message });
  }
};

const find = async (id, res) => {
  try {
    const data = await productModel.findById(id);
    return data;
  } catch (error) {
    res.statut(error.status).json({ error: error.message });
  }
};

const update = async (id, body, res) => {
  try {
    await productModel.findByIdAndUpdate(id, body);

    return true;
  } catch (error) {
    res.statut(error.status).json({ error: error.message });
  }
};

const destroy = async (id, res) => {
  try {
    await productModel.findByIdAndDelete(id);

    return true;
  } catch (error) {
    res.statut(error.status).json({ error: error.message });
  }
};

module.exports = { index, find, update, store, destroy };
