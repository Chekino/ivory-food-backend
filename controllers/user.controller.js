const userService = require("../services/user.service");
const Joi = require("joi");

const userSchema = Joi.object({
  name: Joi.string().min(3).max(30).required(),
  email: Joi.string().min(3).max(200).required().email(),
  password: Joi.string().min(3).max(200).required(),
});

const createUser = async (req, res) => {
  try {
    const { error } = userSchema.validate(req.body);
    if (error) {
      return res.status(400).send(error.details[0].message);
    }

    const token = await userService.createUser(req.body);
    res.send(token);
  } catch (error) {
    res.status(400).send(error.message);
  }
};

module.exports = {
  createUser,
};
