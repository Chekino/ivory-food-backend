const productService = require("../services/product.service");

const index = async (req, res) => {
  try {
    const data = await productService.index();
    res.status(200).json(data);
  } catch (error) {
    res.status(error.status).json({ error: error.message });
  }
};

const find = async (req, res) => {
  try {
    const data = await productService.find(req.params.id);
    res.status(200).json(data);
  } catch (error) {
    res.status(error.status).json({ error: error.message });
  }
};

const store = async (req, res) => {
  try {
    const data = await productService.store(req.body);
    res.status(200).json(data);
  } catch (error) {
    res.status(error.status).json({ error: error.message });
  }
};

const update = async (req, res) => {
  try {
    const data = await productService.update(req.params.id, req.body);
    res.status(200).json(data);
  } catch (error) {
    res.status(error.status).json({ error: error.message });
  }
};

const destroy = async (req, res) => {
  try {
    const data = await productService.destroy(req.params.id);
    res.status(200).json(data);
  } catch (error) {
    res.status(error.status).json({ error: error.message });
  }
};

module.exports = { find, destroy, update, store, index };
