const productService = require("../services/form.service");

const store = async (req, res) => {
  try {
    const data = await productService.store(req.body);
    res.status(200).json(data);
  } catch (error) {
    res.status(error.status).json({ error: error.message });
  }
};

const index = async (req, res) => {
  try {
    const data = await productService.index();
    res.status(200).json(data);
  } catch (error) {
    res.status(error.status).json({ error: error.message });
  }
};
const update = async (req, res) => {
  try {
    const data = await productService.update(req.params.id, req.body);
    res.status(200).json(data);
  } catch (error) {
    res.status(error.status).json({ error: error.message });
  }
};
module.exports = { update, store, index };
